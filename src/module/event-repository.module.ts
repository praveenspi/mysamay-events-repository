import { GSTRepository } from './../repositories/gst-report-repository/gst-repository';
import { BannerRepository } from './../repositories/banner-repository/banner-repository';
import { SequenceRepository } from './../repositories/transaction/sequence-repository';
import { RegistrationEventRepository } from './../repositories/transaction/registration-event-repository';
import { RegistrationRepository } from './../repositories/registration-repository/registration-repository';
import { RegFieldRepository } from './../repositories/reg-field-repository/reg-field-repository';
import { Module } from '@nestjs/common';
import { DbModule } from '@neb-sports/mysamay-db-provider';

import { EventsRepository } from "../repositories/event-repository/event-repository";
import { EventUserMapRepository } from '../repositories/event-user-map/event-user-map-repository';
import { LeaderboardTemplateRepository } from '../repositories/leaderboard-template/leaderboard-template-repository';

@Module({
    imports: [DbModule],
    providers: [
        EventsRepository, 
        EventUserMapRepository, 
        LeaderboardTemplateRepository,
        RegFieldRepository,
        RegistrationRepository,
        RegistrationEventRepository,
        SequenceRepository,
        BannerRepository,
        GSTRepository
    ],
    exports: [
        EventsRepository, 
        EventUserMapRepository, 
        LeaderboardTemplateRepository,
        RegFieldRepository,
        RegistrationRepository,
        RegistrationEventRepository,
        SequenceRepository,
        BannerRepository,
        GSTRepository
    ],
})
export class EventRepositoryModule {}
