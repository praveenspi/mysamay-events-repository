import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { GSTReport } from '@neb-sports/mysamay-events-model';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';

@Injectable()
export class GSTRepository {
    db: Db;
    collection: Collection<GSTReport>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(GSTReport.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createGSTReport(gst: GSTReport): Promise<GSTReport> {
        await this.getCollection();
        let result = await this.collection.insertOne(gst);
        if (result.insertedCount === 1) return result.ops[0];
        return null;
    }

    async updateGSTReport(gst: GSTReport) {
        await this.getCollection();
        await this.collection.updateOne(
            { _id: gst._id },
            gst
        );
    }

    async getGSTReportByAnyQuery(
        query: FilterQuery<GSTReport>,
    ): Promise<GSTReport> {
        await this.getCollection();
        let result = await this.collection.findOne(query, {
        });
        if (result) return result;
        else return undefined;
    }

    async countGSTReportByAnyQuery(
        query: FilterQuery<GSTReport>,
    ): Promise<number> {
        await this.getCollection();
        return await this.collection.countDocuments(query);
    }

    async getGSTReportByAnyQueryPaginated(
        query: FilterQuery<GSTReport>,
        skip: number,
        limit: number,
        sort: SortOptionObject<GSTReport>
    ): Promise<GSTReport[]> {
        await this.getCollection();
        let result = await this.collection
            .find(query)
            .sort(sort)
            .skip(skip)
            .limit(limit)
            .toArray();
        if (result && result.length > 0) return result;
        else return undefined;
    }

    async aggregateGStReport<T>(q: FilterQuery<GSTReport>) {
        await this.getCollection()
        let result = await this.collection.aggregate<T>([
            {$match: q},
            {$group: {_id: {eventName: "$eventName", eventId: "$eventId"}, regFee: {$sum: "$regFee"}, gstAmount: {$sum: "$taxAmount"}, total: {$sum: "$invoiceAmount"}}},
            {$sort: {_id: 1}}
        ]).toArray()
        return result;
    }


}
