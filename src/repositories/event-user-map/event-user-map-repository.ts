import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { EventUserMap } from '@neb-sports/mysamay-events-model';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';

@Injectable()
export class EventUserMapRepository {
    db: Db;
    collection: Collection<EventUserMap>;

    constructor(private dbProvider: MongoProvider) {}

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(EventUserMap.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createEventUserMap(event: EventUserMap): Promise<EventUserMap> {
        try {
            await this.getCollection();
            let result = await this.collection.insertOne(event);
            if (result.insertedCount === 1) return result.ops[0];
            return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async updateEventUserMap(event: EventUserMap): Promise<EventUserMap> {
        try {
            await this.getCollection();
            let result = await this.collection.findOneAndReplace(
                { _id: event._id },
                event,
                { returnOriginal: false },
            );
            if (result.ok === 1) return result.value;
            else return undefined;
        } catch (error) {
            console.log(error);
            Promise.reject(error);
        }
    }

    async getEventUserMapByAnyQuery(
        query: FilterQuery<EventUserMap>,
        projection: SchemaMember<EventUserMap, any>,
    ): Promise<EventUserMap> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne(query, {
                projection: projection,
            });
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async countEventUserMapByAnyQuery(
        query: FilterQuery<EventUserMap>,
    ): Promise<number> {
        try {
            await this.getCollection();
            return await this.collection.countDocuments(query);
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getEventUserMapByAnyQueryPaginated(
        query: FilterQuery<EventUserMap>,
        skip: number,
        limit: number,
        projection: SchemaMember<EventUserMap, any>,
    ): Promise<EventUserMap[]> {
        try {
            await this.getCollection();
            let result = await this.collection
                .find(query, { projection: projection })
                .skip(skip)
                .limit(limit)
                .toArray();
            if (result && result.length > 0) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

   
}
