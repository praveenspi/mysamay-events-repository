import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';

import fs from 'fs';
import path from 'path';
import { FilterQuery } from 'mongodb';

import { EventUserMapRepository } from "./event-user-map-repository";

describe('ActivityRepository', () => {
    let provider: EventUserMapRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [EventUserMapRepository],
        }).compile();

        provider = module.get<EventUserMapRepository>(EventUserMapRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

});
