import { Test, TestingModule } from '@nestjs/testing';
import { LeaderboardTemplateRepository } from './leaderboard-template-repository';
import { DbModule } from "@neb-sports/mysamay-db-provider";

describe('LeaderboardTemplateRepository', () => {
  let provider: LeaderboardTemplateRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DbModule],
      providers: [LeaderboardTemplateRepository],
    }).compile();

    provider = module.get<LeaderboardTemplateRepository>(LeaderboardTemplateRepository);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
