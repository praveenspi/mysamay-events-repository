import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember } from 'mongodb';
import { LeaderboardTemplate } from "@neb-sports/mysamay-events-model";
import { MongoProvider } from '@neb-sports/mysamay-db-provider';

@Injectable()
export class LeaderboardTemplateRepository {
    db: Db;
    collection: Collection<LeaderboardTemplate>;

    constructor(private dbProvider: MongoProvider) {}

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(LeaderboardTemplate.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createTemplate(template: LeaderboardTemplate): Promise<LeaderboardTemplate> {
        try {
            await this.getCollection();
            let result = await this.collection.insertOne(template);
            if (result.insertedCount === 1) return result.ops[0];
            return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async updateTemplate(template: LeaderboardTemplate): Promise<LeaderboardTemplate> {
        try {
            await this.getCollection();
            let result = await this.collection.findOneAndReplace(
                { _id: template._id },
                template,
                { returnOriginal: false },
            );
            if (result.ok === 1) return result.value;
            else return undefined;
        } catch (error) {
            console.log(error);
            Promise.reject(error);
        }
    }

    async getTemplateByAnyQuery(
        query: FilterQuery<LeaderboardTemplate>,
        projection: SchemaMember<LeaderboardTemplate, any>,
    ): Promise<LeaderboardTemplate> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne(query, {
                projection: projection,
            });
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    
    async getTemplateAnyQueryPaginated(
        query: FilterQuery<LeaderboardTemplate>,
        skip: number,
        limit: number,
        projection: SchemaMember<LeaderboardTemplate, any>,
    ): Promise<LeaderboardTemplate[]> {
        try {
            await this.getCollection();
            let result = await this.collection
                .find(query, { projection: projection })
                .skip(skip)
                .limit(limit)
                .toArray();
            if (result && result.length > 0) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

   
}
