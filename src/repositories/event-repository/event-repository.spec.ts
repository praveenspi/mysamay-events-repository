import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';
import { EventsRepository } from './event-repository';

import fs from 'fs';
import path from 'path';
import { FilterQuery } from 'mongodb';

describe('EventsRepository', () => {
    let provider: EventsRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [EventsRepository],
        }).compile();

        provider = module.get<EventsRepository>(EventsRepository);
    });

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

});
