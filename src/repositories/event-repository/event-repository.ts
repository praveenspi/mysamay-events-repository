import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { EventEntity } from '@neb-sports/mysamay-events-model';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';
import { PaginationParams } from '@neb-sports/mysamay-common-utils';

@Injectable()
export class EventsRepository {
    db: Db;
    collection: Collection<EventEntity>;

    constructor(private dbProvider: MongoProvider) {}

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(EventEntity.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createEvent(event: EventEntity): Promise<EventEntity> {
        try {
            await this.getCollection();
            let result = await this.collection.insertOne(event);
            if (result.insertedCount === 1) return result.ops[0];
            return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async updateEvent(event: EventEntity): Promise<EventEntity> {
        try {
            await this.getCollection();
            let result = await this.collection.findOneAndReplace(
                { _id: event._id },
                event,
                { returnOriginal: false },
            );
            if (result.ok === 1) return result.value;
            else return undefined;
        } catch (error) {
            console.log(error);
            Promise.reject(error);
        }
    }

    async getEventByAnyQuery(
        query: FilterQuery<EventEntity>,
        projection: SchemaMember<EventEntity, any>,
    ): Promise<EventEntity> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne(query, {
                projection: projection,
            });
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async countEventByAnyQuery(
        query: FilterQuery<EventEntity>,
    ): Promise<number> {
        try {
            await this.getCollection();
            return await this.collection.countDocuments(query);
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getEventById(id: string): Promise<EventEntity> {
        try {
            await this.getCollection();
            let result = await this.collection.findOne({ _id: id });
            if (result) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async getEventByAnyQueryPaginated(
        query: FilterQuery<EventEntity>,
        skip: number,
        limit: number,
        sort: SortOptionObject<EventEntity>,
        projection: SchemaMember<EventEntity, any>,
    ): Promise<EventEntity[]> {
        try {
            await this.getCollection();
            let result = await this.collection
                .find(query, { projection: projection })
                .skip(skip)
                .limit(limit)
                .sort(sort)
                .toArray();
            if (result && result.length > 0) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

   
}
