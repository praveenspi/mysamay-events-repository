import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { EventRegistration, RegistrationFieldSetup } from '@neb-sports/mysamay-events-model';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';

@Injectable()
export class RegistrationRepository {
    db: Db;
    collection: Collection<EventRegistration>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(EventRegistration.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createEventRegistration(eventReg: EventRegistration): Promise<EventRegistration> {
        await this.getCollection();
        let result = await this.collection.insertOne(eventReg);
        if (result.insertedCount === 1) return result.ops[0];
        return null;
    }

    async updateEventRegistration(eventReg: EventRegistration) {
        await this.getCollection();
        let result = await this.collection.findOneAndReplace(
            { _id: eventReg._id },
            eventReg,
            { returnDocument: "after" }
        );
        return result.value;
    }

    async getEventRegistrationByAnyQuery(
        query: FilterQuery<EventRegistration>,
    ): Promise<EventRegistration> {
        await this.getCollection();
        let result = await this.collection.findOne(query, {
        });
        if (result) return result;
        else return undefined;
    }

    async countEventRegistrationByAnyQuery(
        query: FilterQuery<EventRegistration>,
    ): Promise<number> {
        await this.getCollection();
        return await this.collection.countDocuments(query);
    }

    async getEventRegistrationByAnyQueryPaginated(
        query: FilterQuery<EventRegistration>,
        skip: number,
        limit: number,
    ): Promise<EventRegistration[]> {
        await this.getCollection();
        let result = await this.collection
            .find(query)
            .sort({ fieldKey: 1 })
            .skip(skip)
            .limit(limit)
            .toArray();
        if (result && result.length > 0) return result;
        else return undefined;
    }

    async deleteEventRegistrationByAnyQuery(
        query: FilterQuery<EventRegistration>,
     ) {
        await this.getCollection();
        await this.collection.deleteOne(query);
    }
}
