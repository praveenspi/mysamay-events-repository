import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { EventRegistration, RegistrationEvent, RegistrationFieldSetup } from '@neb-sports/mysamay-events-model';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';

@Injectable()
export class RegistrationEventRepository {
    db: Db;
    collection: Collection<RegistrationEvent>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(RegistrationEvent.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createRegistrationEvent(eventReg: RegistrationEvent): Promise<RegistrationEvent> {
        await this.getCollection();
        let result = await this.collection.insertOne(eventReg);
        if (result.insertedCount === 1) return result.ops[0];
        return null;
    }

    async getRegistrationEventByAnyQuery(
        query: FilterQuery<EventRegistration>,
    ): Promise<RegistrationEvent> {
        await this.getCollection();
        let result = await this.collection.findOne(query, {
          sort: {eventTime: -1}  
        });
        if (result) return result;
        else return undefined;
    }

    async countRegistrationEventByAnyQuery(
        query: FilterQuery<RegistrationEvent>,
    ): Promise<number> {
        await this.getCollection();
        return await this.collection.countDocuments(query);
    }

    async deleteRegistrationEventByAnyQuery(query: FilterQuery<RegistrationEvent>)  {
        await this.getCollection();
        return await this.collection.deleteOne(query)
    }

    async getRegistrationEventByAnyQueryPaginated(
        query: FilterQuery<RegistrationEvent>,
        skip: number,
        limit: number,
    ): Promise<RegistrationEvent[]> {
        await this.getCollection();
        let result = await this.collection
            .find(query)
            .sort({ eventTime: -1 })
            .skip(skip)
            .limit(limit)
            .toArray();
        if (result && result.length > 0) return result;
        else return undefined;
    }


}
