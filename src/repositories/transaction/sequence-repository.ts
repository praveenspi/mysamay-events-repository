import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, FindOneAndUpdateOption } from 'mongodb';

import { Sequence } from '@neb-sports/mysamay-events-model';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';
import { MetadataHelper } from '@neb-sports/mysamay-common-utils';

@Injectable()
export class SequenceRepository {
    db: Db;
    collection: Collection<Sequence>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(Sequence.collectionName);

        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async deleteAll() {
        await this.getCollection();
        await this.collection.deleteMany({});
    }

    async createNewSequence(prefix?: string) {
        await this.getCollection();
        if(!prefix) {
            prefix = "MYS";
        }
        let result = await this.collection.insertOne({
            _id: MetadataHelper.generateUUID(),
            sequence: 1,
            prefix: prefix
        });
        return result.ops[0];
    }

    async getNextSequence(prefix?: string) {
        await this.getCollection();
        let q: FilterQuery<Sequence> = {};
        if(!prefix) {
            prefix = "MYS";
        }
        q.prefix = prefix;

        let opts: FindOneAndUpdateOption<Sequence> = {
            // returnDocument: "after",
            returnOriginal: false

        }

        let result = await this.collection.findOneAndUpdate(q, {$inc: {sequence: 1}}, opts);
        console.log(result.value)
        if(result && result.value) {
            return result.value;
        }
        else {
            // try one more time to create new.
            let createResult = await this.createNewSequence(prefix);
            return createResult;
        }
    }
}
