import { SequenceRepository } from './sequence-repository';
import { DbModule } from '@neb-sports/mysamay-db-provider';
import { Test, TestingModule } from '@nestjs/testing';

describe('ActivityRepository', () => {
    let provider: SequenceRepository;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [SequenceRepository],
        }).compile();

        provider = module.get<SequenceRepository>(SequenceRepository);
    });

    afterEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [DbModule],
            providers: [SequenceRepository],
        }).compile();

        provider = module.get<SequenceRepository>(SequenceRepository);
        await provider.deleteAll()
    })

    it('should be defined', () => {
        expect(provider).toBeDefined();
    });

    it('should create/get new sequence', async () => {
        let result = await provider.createNewSequence();
        expect(result.sequence).toEqual(1)
        result = await provider.getNextSequence();
        expect(result.sequence).toEqual(2);
    })

});
