import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { RegistrationFieldSetup } from '@neb-sports/mysamay-events-model';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';

@Injectable()
export class RegFieldRepository {
    db: Db;
    collection: Collection<RegistrationFieldSetup>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(RegistrationFieldSetup.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createRegistrationField(regField: RegistrationFieldSetup): Promise<RegistrationFieldSetup> {
        await this.getCollection();
        let result = await this.collection.insertOne(regField);
        if (result.insertedCount === 1) return result.ops[0];
        return null;
    }

    async updateRegistrationField(regField: RegistrationFieldSetup) {
        await this.getCollection();
        await this.collection.updateOne(
            { _id: regField._id },
            regField
        );
    }

    async getRegistrationFieldByAnyQuery(
        query: FilterQuery<RegistrationFieldSetup>,
    ): Promise<RegistrationFieldSetup> {
        await this.getCollection();
        let result = await this.collection.findOne(query, {
        });
        if (result) return result;
        else return undefined;
    }

    async countRegistrationFieldByAnyQuery(
        query: FilterQuery<RegistrationFieldSetup>,
    ): Promise<number> {
        await this.getCollection();
        return await this.collection.countDocuments(query);
    }

    async getRegistrationFieldByAnyQueryPaginated(
        query: FilterQuery<RegistrationFieldSetup>,
        skip: number,
        limit: number,
    ): Promise<RegistrationFieldSetup[]> {
        await this.getCollection();
        let result = await this.collection
            .find(query)
            .sort({ fieldKey: 1 })
            .skip(skip)
            .limit(limit)
            .toArray();
        if (result && result.length > 0) return result;
        else return undefined;
    }


}
