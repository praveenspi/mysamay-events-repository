import { Injectable } from '@nestjs/common';
import { Collection, Db, FilterQuery, SchemaMember, SortOptionObject } from 'mongodb';

import { BannerConfig } from '@neb-sports/mysamay-events-model';
import { MongoProvider } from '@neb-sports/mysamay-db-provider';

@Injectable()
export class BannerRepository {
    db: Db;
    collection: Collection<BannerConfig>;

    constructor(private dbProvider: MongoProvider) { }

    async getCollection() {
        if (!this.collection) {
            this.db = await this.dbProvider.getDB(null);
            this.collection = this.db.collection(BannerConfig.collectionName);
        }
        return this.collection;
    }

    async disconnect() {
        if (this.dbProvider.client && this.dbProvider.client.isConnected()) {
            await this.dbProvider.client.close(true);
        }
    }

    async createBannerConfig(banner: BannerConfig): Promise<BannerConfig> {
        await this.getCollection();
        let result = await this.collection.insertOne(banner);
        if (result.insertedCount === 1) return result.ops[0];
        return undefined;
    }

    async updateBanerConfig(banner: BannerConfig): Promise<BannerConfig> {
        await this.getCollection();
        let result = await this.collection.findOneAndReplace(
            { _id: banner._id },
            event,
            { returnOriginal: false },
        );
        if (result.ok === 1) return result.value;
        else return undefined;
    }

    async getBannerByAnyQuery(
        query: FilterQuery<BannerConfig>
    ): Promise<BannerConfig> {
        await this.getCollection();
        let result = await this.collection.findOne(query);
        if (result) return result;
        else return undefined;
    }

    async countBannerByAnyQuery(
        query: FilterQuery<BannerConfig>,
    ): Promise<number> {
        await this.getCollection();
        return await this.collection.countDocuments(query);
    }

    async getBannerByAnyQueryPaginated(
        query: FilterQuery<BannerConfig>,
        skip: number,
        limit: number,
    ): Promise<BannerConfig[]> {
        try {
            await this.getCollection();
            let result = await this.collection
                .find(query)
                .skip(skip)
                .limit(limit)
                .toArray();
            if (result && result.length > 0) return result;
            else return undefined;
        } catch (error) {
            Promise.reject(error);
        }
    }

    async deleteBannerConfig(query: FilterQuery<BannerConfig>) {
        await this.getCollection();
        let result = await this.collection.deleteMany(query)
        if(result.deletedCount > 0) return true;
        return false
        
    }


}
